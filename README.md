# Java test task

## Introduction
In this task you will be simulating the functionality of a search engine, you will implement both the client and the server side. The task focuses on the server programing, which includes web sockets, multithreaded programming and synchronization.

## Description 
There is only one server process and unlimited number of client processes.
A client process sends a search requests or queries to the server through the web sockets in an infinite loop which returns a reply.
In this small simulation a search request will be a number that was chosen randomly, and the response will be also a number.
The server keeps a database of all requests and their replies. The server maintains a set of files in which it saves the numbers (x; y; z): where x is the request, y is its reply, and z is the number of times that x was requested so far. The server keeps track on z in order to maintain a cache of most frequently asked requests and replies.
For each incoming search request the server assigns a separate thread to handle the request and return the reply. Use the thread pool manager to organize and handle these server threads. You are not allowed to use the custom pool manager API supplied by Java, write your own thread pool manager. The number of S-thread the server is able to create is S, where S is a parameter passed to the server process through the command line upon its creation.

C - size of the cache - now configure in AppConfig class - value MAX_SIZE_CACHE

## Configure 
Server and client application from console arguments:

1. S - number of allowed S-threads 
2. M - the least number of times that the query has to be requested in order to enter the cache
3. L - to specify the range [1; L] from which missing replies will be drawn randomly 
4. Y - number of reader and writer threads 
5. N - number of clients 
6. R1,
7. R2 - the integer numbers that specify the range [R1;R2]
8. numOfConnectionsPerClient - how many connections can be raise by one client

## Examples

java -jar example.jar 2 6 10 2 5 1 10 10

## Technology

- Spring-boot
- Spring-data-jpa 
- Hibernate
- Thread-manager (custom)