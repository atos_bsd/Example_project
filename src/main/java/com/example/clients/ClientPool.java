package com.example.clients;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.Getter;
import lombok.NoArgsConstructor;;

@Getter
@NoArgsConstructor
@Service
public class ClientPool{

	private Integer numOfClients, r1, r2, numOfConnectionsPerClient;

	@Autowired
	private Client client;

	public void start() {
		List<Client> poolList = prepareClientList();
		poolList.stream().forEach(Client::start);
	}

	private List<Client> prepareClientList() {
		return IntStream.range(NumberUtils.INTEGER_ZERO, numOfClients).mapToObj(num -> raseNewClient(num))
				.collect(Collectors.toList());
	}

	private Client raseNewClient(int num) {
		return client.newInstance(String.valueOf(num), numOfConnectionsPerClient);
	}

	public void setup(String... args) throws Exception {
		this.numOfClients = Integer.valueOf(args[0]);
		this.r1 = Integer.valueOf(args[1]);
		this.r2 = Integer.valueOf(args[2]);
		this.numOfConnectionsPerClient = Integer.valueOf(args[3]);
		assertTrue(r1 < r2);
	}
}
