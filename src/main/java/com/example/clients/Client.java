package com.example.clients;

import java.util.Random;
import java.util.stream.IntStream;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.example.utils.Constants;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor
@RequiredArgsConstructor
@Component
@Scope("prototype")
public class Client extends Thread {

	@Autowired
	@NonNull
	private ClientPool clientPool;

	private RestTemplate restTemplate = new RestTemplate();

	@NonNull
	private String name;

	@NonNull
	private Integer numMaxConnections;

	public void run() {
		IntStream.range(NumberUtils.INTEGER_ZERO, numMaxConnections).forEach(Connect -> callRestService());
	}

	public Client newInstance(String name, Integer numOfConnectionsPerClient) {
		return new Client(clientPool, name, numOfConnectionsPerClient);
	}

	private void callRestService() {
		Integer x = generateIdNumber();
		log.info("Client<name {}>): send {}:", name, x);
		Integer y = reciveYfromRest(x);
		log.info("Client <{}>: got reply {} for query {}", name, y, x);
	}

	private Integer reciveYfromRest(Integer x) {
		return restTemplate.getForObject(prepareUrl(x), Integer.class);
	}

	private String prepareUrl(Integer id) {
		return String.format(Constants.LOCALHOST + Constants.REST_ADDR+"%s", String.valueOf(id));
	}

	private Integer generateIdNumber() {
		return clientPool.getR1() + new Random().nextInt(Math.abs(clientPool.getR2() - clientPool.getR1()));
	}
}
