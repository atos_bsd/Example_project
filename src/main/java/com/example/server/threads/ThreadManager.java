package com.example.server.threads;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;

@NoArgsConstructor
@Getter
@Setter
public abstract class ThreadManager {

	protected int maxThreads;
	private ArrayBlockingQueue<ThreadWorker> threadWorkersQueue;
	private Integer criteriaMax;

	protected abstract List<ThreadWorker> prepareClientList(int maxThreads);

	@SneakyThrows
	public Integer addTaskToExecute(Integer id, Integer criteria, Integer requested) {
		ThreadWorker worker = threadWorkersQueue.take();
		Integer result;
		worker.addRequest(id, criteria, requested);
		worker.run();
		worker.join();
		result = worker.getAnswer().getCriteria();
		threadWorkersQueue.put(worker);
		return result;
	}

	public void setup(String maxThreadsArg, String criteriaMaxArg) {
		maxThreads = Integer.valueOf(maxThreadsArg);
		criteriaMax = Optional.ofNullable(criteriaMaxArg).map(num -> Integer.valueOf(num)).orElseGet(() -> null);
		threadWorkersQueue = new ArrayBlockingQueue(maxThreads);
		List<ThreadWorker> poolList = prepareClientList(maxThreads);
		poolList.stream().forEach(this::addWorkerToQueue);
	}

	@SneakyThrows
	private void addWorkerToQueue(ThreadWorker worker) {
		threadWorkersQueue.put(worker);
	}

}
