package com.example.server.threads;

import com.example.server.model.Answer;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;

@NoArgsConstructor
@RequiredArgsConstructor
@Setter
public abstract class ThreadWorker extends Thread {

	@NonNull
	protected String nameOfThread;

	@Getter
	protected Answer answer = new Answer();

	@SneakyThrows
	public void run() {
		prepareAnsewer();
	}

	protected abstract void prepareAnsewer();

	public void addRequest(Integer id, Integer criteria, Integer requested) {
		answer = new Answer(id, criteria, requested);
	}

}
