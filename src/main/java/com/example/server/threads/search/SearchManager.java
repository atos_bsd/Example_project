package com.example.server.threads.search;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.server.threads.ThreadManager;
import com.example.server.threads.ThreadWorker;
import com.example.server.threads.read.ReadManager;

@Service
public class SearchManager extends ThreadManager{
		
	@Autowired
	private ReadManager readManager;
	
	private String getName(int num) {
		return String.format("%s_id_%s", SearchWorker.class.getSimpleName(), num);
	}

	@Override
	protected List<ThreadWorker> prepareClientList(int maxThreads) {
		return IntStream.range(NumberUtils.INTEGER_ZERO, maxThreads).mapToObj(num -> new SearchWorker(readManager, getName(num)))
				.collect(Collectors.toList());
	}
}
