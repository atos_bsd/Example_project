package com.example.server.threads.search;

import org.springframework.stereotype.Component;

import com.example.server.threads.ThreadWorker;
import com.example.server.threads.read.ReadManager;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@NoArgsConstructor
@Component
@Slf4j
public class SearchWorker extends ThreadWorker {

	private ReadManager readManager;

	public SearchWorker(ReadManager readManager, String name) {
		super(name);
		this.readManager = readManager;
	}

	protected synchronized void prepareAnsewer() {
		log.info("Fetch from DB Id - {}, worker - {}", answer.getId(), nameOfThread);
		Integer criteria = readManager.addTaskToExecute(answer.getId(), null,null);
		answer.setCriteria(criteria);
	}
}
