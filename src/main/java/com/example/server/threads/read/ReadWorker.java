package com.example.server.threads.read;

import java.util.Optional;
import java.util.Random;

import org.apache.commons.lang3.math.NumberUtils;

import com.example.server.model.Answer;
import com.example.server.repository.AnswerRepositoryService;
import com.example.server.threads.ThreadWorker;
import com.example.server.threads.write.WriteManager;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@NoArgsConstructor
@Slf4j
public class ReadWorker extends ThreadWorker {

	private AnswerRepositoryService answerRepositoryService;
	private ReadManager readManager;
	private WriteManager writeManager;

	public ReadWorker(WriteManager writeManager, ReadManager readManager,
			AnswerRepositoryService answerRepositoryService, String name) {
		super(name);
		this.readManager = readManager;
		this.answerRepositoryService = answerRepositoryService;
		this.writeManager = writeManager;
	}

	protected void prepareAnsewer() {
		synchronized (answerRepositoryService) {
			log.info("Fetch from DB Id - {}, worker - {}", answer.getId(), nameOfThread);
			Answer answerRecived = getAnswerFromRepository(answer.getId())
					.orElseGet(() -> generateNewCriteria(answer.getId()));
			answer.setCriteria(answerRecived.getCriteria());
		}
	}

	private Optional<Answer> getAnswerFromRepository(Integer id) {
		return Optional.ofNullable(answerRepositoryService.findAnswerById(id))
				.map(answer -> incriseRequestedCount(answer));
	}

	private Answer incriseRequestedCount(Answer answer) {
		writeManager.addTaskToExecute(answer.getId(), answer.getCriteria(),
				Integer.valueOf(answer.getRequested().intValue() + 1));
		return answer;
	}

	private Answer generateNewCriteria(Integer id) {
		log.info("Ganarate new criteria for id - {}", id);
		Integer criteria = NumberUtils.INTEGER_ONE
				+ new Random().nextInt(Math.abs(readManager.getCriteriaMax() - NumberUtils.INTEGER_ONE));
		writeManager.addTaskToExecute(id, criteria, NumberUtils.INTEGER_ZERO);
		return new Answer(id, criteria, NumberUtils.INTEGER_ZERO);
	}
}
