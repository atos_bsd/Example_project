package com.example.server.threads.write;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.server.repository.AnswerRepository;
import com.example.server.threads.ThreadManager;
import com.example.server.threads.ThreadWorker;

@Service
public class WriteManager extends ThreadManager {

	@Autowired
	private AnswerRepository answerRepository;

	private String getName(int num) {
		return String.format("%s_id_%s", WriteWorker.class.getSimpleName(), num);
	}

	@Override
	protected List<ThreadWorker> prepareClientList(int maxThreads) {
		return IntStream.range(NumberUtils.INTEGER_ZERO, maxThreads)
				.mapToObj(num -> new WriteWorker(answerRepository, getName(num))).collect(Collectors.toList());
	}
	
	
}
