package com.example.server.threads.write;

import org.apache.commons.lang3.math.NumberUtils;

import com.example.server.repository.AnswerRepository;
import com.example.server.threads.ThreadWorker;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@NoArgsConstructor
@Slf4j
public class WriteWorker extends ThreadWorker {

	private AnswerRepository answerRepository;

	public WriteWorker(AnswerRepository answerRepository, String name) {
		super(name);
		this.answerRepository = answerRepository;
	}

	protected void prepareAnsewer() {
		log.info("Write from DB Id - {}, worker - {}", answer.getId(), nameOfThread);
		
		if(answer.getRequested()>=NumberUtils.INTEGER_ONE)
			answerRepository.updateAnswer(answer.getId(), answer.getRequested());
		else
			answerRepository.insertNewAnswer(answer.getId(), answer.getCriteria(),answer.getRequested());
	}
}
