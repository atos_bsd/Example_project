package com.example.server.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.server.threads.search.SearchManager;
import com.example.utils.Constants;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class HelloController {

    
    @Autowired
    private SearchManager searchManager;

    @RequestMapping("/")
    String hello() {
        return "Hello!";
    }

    @RequestMapping(value = Constants.REST_ADDR+"{id}", method = RequestMethod.GET)
    public Integer sample(@PathVariable("id") int id) {
        log.info("Start get Answer. ID=" + id);
        return searchManager.addTaskToExecute(id,null,null);
    }


}
