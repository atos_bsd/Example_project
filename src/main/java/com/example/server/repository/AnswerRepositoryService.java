package com.example.server.repository;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.example.server.model.Answer;

import lombok.Setter;

@Service
@Setter
public class AnswerRepositoryService {

	@Autowired
	private AnswerRepository answerRepository;

	public Integer numberOfQueryRequested;

	@Cacheable(value = "answer", key = "#root.args[0]", unless = "#result==null||#root.target.numberOfQueryRequested>#result.requested")
	public Answer findAnswerById(Integer id) {
		return Optional.ofNullable(answerRepository.findById(id)).orElseGet(() -> null);
	}

}
