package com.example.server.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.example.server.model.Answer;

public interface AnswerRepository extends Repository<Answer, Integer> {

	public Answer findById(Integer id);

	@Modifying
	@Transactional
	@Query(value = "insert into Answer (id, criteria,requested) values (:id, :criteria,:requested)", nativeQuery = true)
	public void insertNewAnswer(@Param("id") Integer id, @Param("criteria") Integer criteria,
			@Param("requested") Integer requested);
	
	@Modifying
	@Transactional
	@Query(value = "update  Answer a set a.requested=:requested where a.id=:id", nativeQuery = true)
	public void updateAnswer(@Param("id") Integer id, @Param("requested") Integer requested);
}
