package com.example.server.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Answer {
	
	  @Id
	  @GeneratedValue(strategy = GenerationType.AUTO)
	  @Column(nullable = false)
	  private int id;
	  
	  @Column
	  private Integer criteria;
	  
	  @Column
	  private Integer requested;
}
