package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.example.clients.ClientPool;
import com.example.server.repository.AnswerRepositoryService;
import com.example.server.threads.read.ReadManager;
import com.example.server.threads.search.SearchManager;
import com.example.server.threads.write.WriteManager;

@SpringBootApplication
public class App implements CommandLineRunner {

	@Autowired
	private ClientPool clientPool;

	@Autowired
	private SearchManager searchManager;

	@Autowired
	private ReadManager readManager;

	@Autowired
	private WriteManager writeManager;

	@Autowired
	private AnswerRepositoryService answerRepositoryService;

	public static void main(String[] args) {
		ApplicationContext appContext = SpringApplication.run(App.class, args);
		ClientPool clientPoolBean = appContext.getBean(ClientPool.class);
		clientPoolBean.start();
	}

	/**
	 * @description - configure server and client application from console arguments
	 * @author Alex Rusin
	 * @param: 	0. S - number of allowed S-threads 
	 * 			   C - size of the cache - now configure in AppConfig class - value MAX_SIZE_CACHE
	 * 			1. M - the least number of times that the query has to be
	 *             requested in order to enter the cache 
	 *          2. L - to specify the range [1; L] from which missing replies will be drawn
	 *             randomly 
	 *          3. Y - number of reader and writer threads 
	 *          4. N - number of clients 
	 *          5. R1,
	 *          6. R2 - the integer numbers that specify the range [R1;R2]
	 * 			7. numOfConnectionsPerClient - how many connections can be raise by one client
	 */
	@Override
	public void run(String... args) throws Exception {
		
		searchManager.setup(args[0], null);
		answerRepositoryService.setNumberOfQueryRequested(Integer.valueOf(args[1]));
		readManager.setup(args[3],args[2]);
		writeManager.setup(args[3], null);
		clientPool.setup(args[4], args[5], args[6], args[7]);
	}
}