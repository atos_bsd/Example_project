package com.example.configs;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import liquibase.integration.spring.SpringLiquibase;

@Configuration
@EnableAutoConfiguration(exclude = LiquibaseAutoConfiguration.class)
public class LiquibaseConfig {
	
    @Autowired
    private DataSource dataSource;
	
	@Bean
	public SpringLiquibase liquibase() {
	    SpringLiquibase liquibase = new SpringLiquibase();
	    liquibase.setChangeLog("classpath:/liquibase/liquibase-diff-changeLog.xml");
	    liquibase.setDataSource(dataSource);
	    liquibase.setDropFirst(false);
	    return null;
	}

}