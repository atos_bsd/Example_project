package com.example.configs;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.cache.CacheBuilder;

@Configuration
@EnableCaching
public class AppConfig {

	private static final int MAX_SIZE_CACHE = 1000;

	@Bean
	public CacheManager cacheManager() {
		GuavaCacheManager cacheManager = new GuavaCacheManager("answer");
		CacheBuilder<Object, Object> cacheBuilder = CacheBuilder.newBuilder().maximumSize(MAX_SIZE_CACHE);
		cacheManager.setCacheBuilder(cacheBuilder);
		return cacheManager;
	}

}
